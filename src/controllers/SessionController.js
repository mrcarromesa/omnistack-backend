const User = require("../models/User");

//metodos:
//index, show, store, update, destroy
module.exports = {
    //executar funcao asyncrona combinada com await
    async store(req, res) {

        //recurso do js chamado desetruturacao;
        const { email } = req.body;

        let user = await User.findOne({email});

        if (!user) {
            user = await User.create({email}); // so ira para proxima linha apos executar completamente essa linha, await precisa do async na funcao.
        }

        return res.json(user);
    }
}