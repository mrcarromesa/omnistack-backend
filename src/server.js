//yarn init
//yarn add express
//yarn add nodemon -D //-D = Developer add in package no modo developer
//abrir o package e add o seguinte:
/*
"scripts": {
    "dev": "nodemon src/server.js"
  },
*/
//rodar: yarn dev //para executar o nodemon 
//yarn add mongoose //para instalar o manipulador do mongodb

//mongodb atlas -- criar base de dados mongodb na nuvem
//yarn add multer // para aceitar upload de arquivos

//yarn add cors // dependcia para aceitar requisicoes fora do mesmo endereco

const express = require("express");
const rotutes = require("./routes");
const mongoose = require("mongoose");
const path = require('path');
const socketio = require('socket.io');

const http = require('http');

const cors = require("cors");

const app = express();
const server = http.Server(app);
const io = socketio(server);


mongoose.connect("mongodb+srv://omnistack:omnistack@omnistack-i2nnb.mongodb.net/semana09?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

const connectedUsers = {};

io.on('connection', socket => {
  const {user_id} = socket.handshake.query;
  connectedUsers[user_id] = socket.id;

  console.log('cod '+connectedUsers[user_id]);
});

//next quando chamar indica que quer receber o fluxo normal da aplicacao
//important que seja exeuctado antes dos demais codigos abaixo dele
app.use((req, res, next) => {
  //como todoas as rotas tem acesso ao req, consigo acessar a propriedade
   req.io = io;
   req.connectedUsers = connectedUsers;

   //muito important se nao a aplicacao para
   return next();
});


//req.query = Acessar query params (para filtros e consultas)
//req.params = Acessar route params (para edição, delete)
//req.body = Acessar corpo da requição (para criação e edição)
app.use(cors());
app.use(express.json());
app.use('/files', express.static(path.resolve(__dirname,'..', 'uploads')));
app.use(rotutes);

//mongodb+srv://omnistack:<password>@omnistack-i2nnb.mongodb.net/test?retryWrites=true&w=majority

server.listen(3333);